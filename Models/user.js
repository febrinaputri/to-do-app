var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    firstname : {type: String,
                required: true,
                max: 100},
    lastname : {type: String,
                required: true,
                max: 100},
    username : {type: String,
                unique: true,
                required: true,
                max: 100},
    email: {type: String, 
            required: true, 
            max: 100,
            lowercase: true,
            unique: true,
            required : [true, 'can\'t be left blank'], 
            match: [/\S+@\S+\.\S+/, 'is invalid'], 
            index: true},
    password: {type: String, 
               required: true, 
               min:6},
    confirmpassword : {type: String,
                       required: true,
                       min: 6}
    },
    {timestamps : true});

userSchema.plugin(uniqueValidator);
module.exports = mongoose.model('user', userSchema)