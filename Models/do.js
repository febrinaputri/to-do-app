var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema= mongoose.Schema;
var doSchema = new Schema({
    userId : {type : Schema.Types.ObjectId, ref: 'user'},
    date : {type : Date},
    title : {type: String, max: 100},
    description : {type: String, max : 100}},
    {timestamps: true}
);

mongoose.plugin(uniqueValidator);
module.exports = mongoose.model('do', doSchema);