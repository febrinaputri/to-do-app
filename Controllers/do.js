var Do = require('../Models/do');

exports.test = (req, res)=>{
    res.status(200).json({
        success : true,
        message : "Testing To Do List controller"
    })
};

exports.doCreate = (req, res) => {
    let newTask = new Do ({
        userId : req.decoded._id,
        date : req.body.date,
        title : req.body.title,
        description : req.body.description
    });
    newTask.save()
    .then((task)=> {
        res.status(201).json({
            success : true,
            message : "The do's list is successfully added",
            data : newTask
        })
    })
    .catch((err)=>{
        res.status(422).json({
            success : false,
            message : err.message
        });
    })
}

exports.doAll = (req,res) => {
    Do.find({userId : req.decoded._id})
    .then((Do)=> {
        console.log(Do)
        res.status(200).json({
            success : true,
            message : "The corresponding user's lists of do's are found",
            data : Do
        })})
    .catch ((err) => {
        res.status(422).json({
            success : false,
            message : err.message
        })
    })
};

exports.doUpdate = (req, res)=>{
    console.log(req.body)
    Do.findByIdAndUpdate(req.params.id, {$set : req.body}, (err, Do)=>{
        if(err){
            res.status(400).json({
                success : false,
                message : err.message
            })
        }
        console.log(Do)
        res.status(200).json({
            success : true,
            message : "The corresponding do's list is successfully updated",
            data : Do
        })
    })
};

exports.doDelete = (req, res)=>{
    Do.findByIdAndDelete(req.params.id, (err, Do)=>{
        if(err){
          res.status(422).json({
              success : false,
              message : err.message
          })
        } else{
          res.status(200).json({ 
              success : true,
              message: "The task with the corresponding id is deleted",
              do : Do})
        }
})};