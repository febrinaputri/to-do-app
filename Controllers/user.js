let user = require('../Models/user');      
var userSchema = require('../Schemas/user')
var bcrypt = require('bcrypt');
const saltRounds = 10;
const Ajv = require('ajv');
var ajv = new Ajv();
var validate = ajv.compile(userSchema);
var jwt = require('jsonwebtoken');

exports.test = (req, res)=>{
    res.json({
        success : true,
        message : "Testing user controller"
    })
};

exports.userCreate = (req, res, next) => {
    let User = new user({
        firstname : req.body.firstname,
        lastname : req.body.lastname,
        username : req.body.username,
        email : req.body.email,
        password : req.body.password,
        confirmpassword : req.body.confirmpassword
    });

    var valid = validate (User);

    if (valid){
    console.log('User data is valid!'+ User.password);
    bcrypt.hash(User.password, saltRounds)
    .then(function (hash) {
      User.password = hash
      User.confirmpassword = hash
      User.save(function (err) {
        if (err) {
          res.status(422).json({
            success : false,
            message : err.message
          });
        }
        res.status(201).json({
          success : true,
          message : 'User created',
          data : User 
          })
        })
      })
    .catch((err) => {
      res.status(500).json({ 
        success: false, 
        message: err.message 
      });
    });;
  } else {
    console.log('User data is invalid!', validate.errors);
    res.status(400).json({
      success : true,
      message: 'Data invalid',
      data : validate.errors 
    })
  }
}

exports.login = (req, res) => {
  let User = user.findOne({email: req.body.email}, function (err, obj){
      if(!err){
          return obj
      }else{
          console.log(err)
          res.status(422).json({
            success : false,
            message : err.message
          })
      }
  })
  
  User.then((User) => {
      console.log(User)
      console.log(req.body)
      bcrypt.compare(req.body.password, User.password)
      .then(function(result){
          if(result){
              var token = jwt.sign(User.toJSON(), process.env.SECRET_KEY,
              {algorithm: 'HS256'})
              // console.log('Successful:' + token)
              res.status(200).json({
                username: User.username,
                email : User.email,
                token: token
              })
          }else{
              res.status(401).json({
                  success : true,
                  message: err.message
              })
          }
      }).catch((err) => res.status(422).json({
        success : false,
        message : err.message
      }))
  })
}

exports.userDetail = (req,res) => {
  console.log(req.decoded);
  user.findById(req.decoded._id)
  .exec()
  .then((user)=>{
    console.log(user);
        res.status(200).json({
      success : true,
      message : "The corresponding user's data is found",
      data : user})
    })
    .catch((err)=>{
      console.log(err)
          res.status(400).json({
          success : false,
          message : err.message
        })
    })
}

exports.userUpdate = (req, res)=>{
  console.log(req.body)
  user.findByIdAndUpdate(req.decoded._id, {$set:req.body}, (err, user)=>{
    if (err){
      res.status(422).json({
        success : false,
        message : err.message
      })
    }
    console.log(user)
    bcrypt.hash(user.password, saltRounds)
    .then(function (hash){
      user.password = hash
      user.confirmpassword = hash
      user.save(function(err){
        if (err){
          res.status(422).json({
            success : false,
            message : err.message
          });
        }
        res.status(201).json({
          success : true,
          message : "The corresponding user's information is successfully updated",
          data : user
        });
      })
    })
    .catch((err)=>{
      res.status(500).json({
        success : false,
        message : err.message
      })
    })
  })
}

exports.userDelete = (req, res)=>{
user.findByIdAndDelete(req.decoded._id, (err, user)=>{
    if(err){
      res.json({
        success : false,
        message : err.message
      })
    } else{
      res.json({ 
        success : true,
        message: "User with the corresponding id is deleted",
        data: user})
    }
})};