const express= require('express');
var cors = require('cors');
const mongoose= require('mongoose');
const app = express();
const createError = require('http-errors');

require('dotenv').config();
var mongoDB = process.env.MONGODB_URL;
const port = process.env.PORT;

const userRouter = require('./Routes/user');
const doRouter = require('./Routes/do');

mongoose.connect(mongoDB, {useNewUrlParser : true});
mongoose.Promise = global.Promise;
var db = mongoose.connection; 
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors())
app.use('/api/v1/user', userRouter);
app.use('/api/v1/do', doRouter);

app.listen(port, () => {
  console.log('Server is up and running on port number ' + port)
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});
// error handler
app.use((err, req, res, next) => {
// set locals, only providing error in development
res.locals.message = err.message;
res.locals.error = process.env.NODE_ENV === 'development' ? err : {};
// render the error page
res.status(err.status || 500);
res.json({success: false,err});
});

module.exports = app;

