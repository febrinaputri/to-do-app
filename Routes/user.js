var express = require('express');
var router = express.Router();
var auth = require('../middleware/auth');
const userController = require('../Controllers/user');

router.get('/test', userController.test);
router.post('/create', userController.userCreate);
router.post('/login', userController.login);
router.get('/detail', auth.isAuthenticated, userController.userDetail)
router.put('/update', auth.isAuthenticated, userController.userUpdate)
router.delete('/delete', auth.isAuthenticated, userController.userDelete);

module.exports= router;