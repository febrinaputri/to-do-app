var express = require('express');
var router = express.Router();
var auth= require('../middleware/auth');
const doController = require('../Controllers/do');

router.get('/test', auth.isAuthenticated, doController.test);
router.post('/create', auth.isAuthenticated, doController.doCreate);
router.get('/all', auth.isAuthenticated, doController.doAll);
router.put('/:id/update', auth.isAuthenticated, doController.doUpdate);
router.delete('/:id/delete', auth.isAuthenticated, doController.doDelete);

module.exports= router;